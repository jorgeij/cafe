import socket
import pyaudio
import pickle
import struct
import threading
import time
import cv2


class Client:
# connects to server socket and sends video stream

    # Initialize object
    def __init__(self):

        # Each stream is connected to a specific socket
        stream_1 = 0
        stream_2 = 0
        stream_3 = 0

        self.streams = {'stream_1':stream_1, 'stream_2':stream_2, 'stream_3':stream_3}

        self.running = True

        size = 1024
        FORMAT = pyaudio.paInt16
        CHANNELS = 1
        RATE = 44100 
        audio = pyaudio.PyAudio()
        ip = "10.22.224.165" # Change this to <host_ip> of server

        self.args = {"size":size, "format":FORMAT, "channels": CHANNELS, "rate":RATE, "audio":audio, "ip":ip}


    # Connect to the correct sockets
    def connect(self):
        thread_audio_receiving = threading.Thread(target = self.receive_audio, args=(5000,)).start()

        thread_audio_sending = threading.Thread(target = self.send_audio, args=(5001,)).start()

        thread_video_receiving = threading.Thread(target = self.receive_video,  args=(8000,"stream_1",)).start()

        thread_video_playing = threading.Thread(target = self.play_video).start()

        time.sleep(2)

        thread_video_sending = threading.Thread(target = self.send_video, args=()).start()

    # disconnect from end opencv and audio streams
    def disconnect(self):
        self.running = False
        print("stopping video")

    # Record and send live video
    def send_video(self): 
        client_socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        host_ip = self.args['ip']
        port = 10000
        print("Socket Created Successfully")
        client_socket.connect((host_ip,port))
        print("Socket Accepted")
        encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), 50]
        while self.running:
            if client_socket:
                vid = cv2.VideoCapture(0)
                vid.set(3,200)
                vid.set(4,200)
                vid.set(5,40)
                while(vid.isOpened()):
                    img,frame = vid.read()

                    result, frame = cv2.imencode('.jpg', frame, encode_param)
                    a = pickle.dumps(frame)
                    message = struct.pack("Q",len(a))+a
                    client_socket.sendall(message)
                    
        client_socket.close()
        vid.release()
        cv2.destroyAllWindows()


    #connects to socket that the server uses to forward the stream from other clients
    def receive_video(self, port, stream):
        client_socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        host_ip = self.args['ip']
        print("Socket Created Successfully")
        client_socket.connect((host_ip,port))
        print("Socket Accepted")

        data = b""
        payload_size = struct.calcsize("Q")
        if client_socket:
            
            while self.running:
                while len(data) < payload_size:
                    packet = client_socket.recv(2160) 
                    if not packet: break
                    data+=packet
                packed_msg_size = data[:payload_size]
                data = data[payload_size:]
                msg_size = struct.unpack("Q",packed_msg_size)[0]
                
                while len(data) < msg_size:
                    data += client_socket.recv(2160)

                self.streams[stream] = data[:msg_size]
                data  = data[msg_size:]
            client_socket.close()

    # Play video based on incoming streams, each streams is connected to a specific socket
    def play_video(self):

        self.running = True

        while self.running:
            
            try:
                pickle_stream_1 = pickle.loads(self.streams['stream_1'])
                decode_stream_1 = cv2.imdecode(pickle_stream_1, cv2.IMREAD_COLOR)
                cv2.imshow("stream_1",decode_stream_1)
                key = cv2.waitKey(1)
                if key  == ord('q'):
                    self.running = False
                    cv2.destroyAllWindows()
                    break

            except: 
                pass  
            
            try:
                pickle_stream_2 = pickle.loads(self.streams['stream_2'])
                decode_stream_2 = cv2.imdecode(pickle_stream_2, cv2.IMREAD_COLOR)
                cv2.imshow("stream_2",decode_stream_2)
                key = cv2.waitKey(1)
                if key  == ord('q'):
                    self.running = False
                    cv2.destroyAllWindows()
                    break
            except: 
                pass  

            try:
                pickle_stream_3 = pickle.loads(self.streams['stream_3'])
                decode_stream_3 = cv2.imdecode(pickle_stream_3, cv2.IMREAD_COLOR)
                cv2.imshow("stream_3",decode_stream_3)
                key = cv2.waitKey(1)
                if key  == ord('q'):
                    self.running = False
                    cv2.destroyAllWindows()
                    break
            except: 
                pass
        cv2.destroyAllWindows()  


    # connects to server socket and sends audio stream
    def send_audio(self, port): 

        recording_stream = self.args["audio"].open(format=self.args["format"],
                                    channels=self.args["channels"],
                                    rate=self.args["rate"],
                                    input=True,
                                    frames_per_buffer=self.args["size"])
        client_socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        host_ip = self.args["ip"]
        print("Socket Created Successfully")
        client_socket.connect((host_ip,port))
        print("Socket Accepted")
        while self.running:
            data = recording_stream.read(self.args["size"])
            if client_socket:
                client_socket.sendall(data)
        
        recording_stream.stop_stream()
        recording_stream.close()
        client_socket.close()

    #connects to socket that the server uses to forward the stream from other clients
    def receive_audio(self, port):
        playing_stream = self.args["audio"].open(format=self.args["format"],
                                    channels=self.args["channels"],
                                    rate=self.args["rate"],
                                    output=True,
                                    frames_per_buffer=self.args["size"])
        client_socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        host_ip = self.args["ip"]
        print("Socket Created Successfully")
        client_socket.connect((host_ip,port))
        print("Socket Accepted")

        if client_socket:
            while self.running:
                try:
                    audioData = client_socket.recv(self.args["size"])
                    playing_stream.write(audioData)
                except socket.error:
                    print('Error, exiting')
                    playing_stream.stop_stream()
                    playing_stream.close()
                    break      
            client_socket.close()
            playing_stream.stop_stream()
            playing_stream.close()                          

           