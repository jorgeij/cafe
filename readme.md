<br />
<div align="center">
  <a href="https://github.com/github_username/repo_name">
    <img src="./UI/img/logo.gif" alt="Logo" width="150" height="150">
  </a>

<h3 align="center">Coffee Break</h3>

  <p align="center">
    Semester project for the course TTM4115
    <br />
  </p>
</div>


## About The Project
This is a Project in the Course TTM4115, where we were tasked with creating a supplement to casual conversations at the place. Our solution was a meeting app where employees could talk with other colleagues during breaks.



### Built With

* [AppJar](http://appjar.info/)
* [STMPY](https://falkr.github.io/stmpy/)
* [MQTT](https://github.com/eclipse/paho.mqtt.python)



## Getting Started

This is an example of how you may give instructions on setting up your project locally.
To get a local copy up and running follow these simple example steps.

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.stud.idi.ntnu.no/jorgeij/cafe
   ```
2. Install python packages packages
   ```sh
    pip install -r requirements.txt
   ```

3. Run the backend server
    ```sh
    python video_server.py <host_ip>
    python audio_server.py <host_ip>
    ```
4. Update the host_ip of the server in last_video_client
    ```sh
    line 29: ip = "<host_ip>"
    ```
5. Run the home client user interface
    ```sh
    python atHome_state_machine.py
    ```

6. Run the coffee client user interface
    ```sh
    python coffee_state_machine.py
    ```

### Installation issues 

1. Linux: 
We had an issue when we were installing pyaudio without specific dependencies. 

```sh 
sudo apt-get install portaudio19-dev 
```

2. Windows:
There were problems with installing pyaudio directly over pip. Installing over pipwin helped this issue.
```sh
  pip install pipwin
  pipwin install pyaudio
```