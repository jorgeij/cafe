from stmpy import Machine, Driver
from threading import Thread
import paho.mqtt.client as mqtt
from UI.GUI import GUI
import client


class MQTT_Client_1:
    def __init__(self):
        self.count = 0
        self.client = mqtt.Client()
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message

    def on_connect(self, client, userdata, flags, rc):
        print("on_connect(): {}".format(mqtt.connack_string(rc)))

    def on_message(self, client, userdata, msg):
        print("on_message(): topic: {}".format(msg.topic))
        if msg.topic == 'ttm4115/team08/invitation':
            self.stm_driver.send('invite_received', 'coffee_client')

        elif msg.topic == 'ttm4115/team08/available_users':
            interface.add_name(str(msg.payload)[2:-1] + " is available")

        elif msg.topic == 'ttm4115/team08/unavailable_users':
            interface.add_name(str(msg.payload)[2:-1] + " is unavailable")

    def start(self, broker, port):
        print("Connecting to {}:{}".format(broker, port))
        self.client.connect(broker, port)
        self.client.subscribe('ttm4115/team08/invitation')
        self.client.subscribe('ttm4115/team08/available_users')
        self.client.subscribe('ttm4115/team08/unavailable_users')

        try:
            thread = Thread(target=self.client.loop_forever)
            thread.start()
        except KeyboardInterrupt:
            print("Interrupted")
            self.client.disconnect()

class CoffeeClient:

    def __init__(self):
        print('init!!')
        self.client = client.Client()

    def print_start(self):
        print('start')

    def print_state_idle_available(self):
        print('idle_available')

    def print_state_idle_unavailable(self):
        print('idle_unavailable')

    def print_state_idle_invited(self):
        print('idle_invited')

    def print_state_invited(self):
        print('invited')

    def print_state_on(self):
        print('on')

    def connect_mqtt(self):
        broker, port = "mqtt.item.ntnu.no", 1883
        myclient.start(broker, port)

    def unsubscribe_mqtt(self):
        myclient.client.unsubscribe('ttm4115/team08/invitation')
        myclient.client.publish("ttm4115/team08/unavailable_users", interface.name)
    
    def subscribe_mqtt(self):
        myclient.client.subscribe('ttm4115/team08/invitation')
        myclient.client.publish("ttm4115/team08/available_users", interface.name)

    def show_invite(self):
        interface.show_subwindow("Incoming Call")
    
    def hide_invite(self):
        interface.hide_subwindow("Incoming Call")

    def show_video_control(self):
        interface.show_subwindow("Video control")

    def hide_control_video(self):
        interface.hide_subwindow("Video control")

    def run_video(self):
        print("Execute video")
        self.client.connect()

    def disconnect_call(self): 
        self.client.disconnect()


coffee_client = CoffeeClient()

# Transitions
t0 = {'source': 'initial',
      'target': 'start',
      'effect': '__init__'}

t1 = {'trigger': 'enter',
      'source': 'start',
      'target': 'idle_available'}

t2 = {'trigger': 'set_unavailable',
      'source': 'idle_available',
      'target': 'idle_unavailable'}

t3 = {'trigger': 'set_available',
      'source': 'idle_unavailable',
      'target': 'idle_available'}

t4 = {'trigger': 'invite_received',
      'source': 'idle_available',
      'target': 'invited'}

t5 = {'trigger': 'accept_invite',
      'source': 'invited',
      'target': 'on'}

t6 = {'trigger': 'decline_invite',
      'source': 'invited',
      'target': 'idle_available'}

t7 = {'trigger': 't',
      'source': 'invited',
      'target': 'idle_available'}

t8 = {'trigger': 'create_invitation',
      'source': 'idle_available',
      'target': 'on'}

t9 = {'trigger': 'hangup_call',
      'source': 'on',
      'target': 'idle_available'}


# States
start = {'name': 'start',
         'entry': 'print_start; connect_mqtt'}

idle_available = {'name': 'idle_available',
                  'entry': 'print_state_idle_available; subscribe_mqtt'}

idle_unavailable = {'name': 'idle_unavailable',
                    'entry': 'print_state_idle_unavailable; unsubscribe_mqtt'}

invited = {'name': 'invited',
           'entry': 'print_state_invited; start_timer("t", 10000); show_invite',
           'exit': 'hide_invite'}

on = {'name': 'on',
      'entry': 'print_state_on; unsubscribe_mqtt; run_video; show_video_control',
      'exit': 'disconnect_call; hide_control_video'}


# Pass the set of states to the state machine
machine = Machine(name='coffee_client', transitions=[
                  t0, t1, t2, t3, t4, t5, t6, t7, t8, t9], obj=coffee_client, states=[idle_available, idle_unavailable, invited, on, start])
coffee_client.stm = machine


myclient = MQTT_Client_1()
driver = Driver()
driver.add_machine(machine)
coffee_client.mqtt_client = myclient.client
myclient.stm_driver = driver
driver.start()

# Creating the interface with GUI and the state machine
interface = GUI(machine, myclient.client)
interface.start()
