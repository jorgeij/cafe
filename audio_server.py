import socket
import sys
import cv2
import pickle
import struct
import time
import threading 
import queue


senders_clients_sockets = []
receivers_clients_sockets = []
temporary_receivers = []
stream_queues = []


def create_socket_receiver(port,ip): # creates socket that receives data from clients and sends to resend_audio()
    server_socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    socket_address = (ip,port)
    print("Socket Created Successfully")

    server_socket.bind(socket_address)
    print("Socket Bind Successfully")

    server_socket.listen(10)
    print("LISTENING AT:",socket_address)

    while True:   
        sender_socket,addr = server_socket.accept()    
        print('GOT SENDER FROM:',addr)    # new client sender
        if receivers_clients_sockets:

            #creates queue where audio streaming will be saved
            thread_new_connection = threading.Thread(target = queue_audio, args=(sender_socket,)).start()
           
            for receiver in receivers_clients_sockets[:]:    
                if have_not_same_ip(receiver,sender_socket) and is_not_receiving(receiver):
                    receivers_clients_sockets.remove(receiver)                                                                                                         
                    temporary_receivers.append(receiver)

                    #each receiver will get audio streaming from the queues
                    thread_new_connection = threading.Thread(target = forward_audio, args=(sender_socket,receiver)).start()
 
            temporary_receivers.clear()

        if senders_clients_sockets:      # clients that joined before, send data to the new receiver
            for sender in senders_clients_sockets:
                
                new_receiver = find_socket_receiver_client(sender_socket)
                receivers_clients_sockets.remove(new_receiver)

                #each receiver socket gets a queue from the senders
                thread_new_connection = threading.Thread(target = forward_audio, args=(sender,new_receiver)).start()

        senders_clients_sockets.append(sender_socket)

 
    
def create_socket_sender(port,ip):   # creates a socket to clients who want the data stream
    server_socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    socket_address = (ip,port)
    print("Socket Created Successfully")

    server_socket.bind(socket_address)
    print("Socket Bind Successfully")

    server_socket.listen(20)
    print("LISTENING AT:",socket_address)

    while True:   
        client_socket,addr = server_socket.accept()    
        print('GOT RECEIVER FROM:',addr)
        receivers_clients_sockets.append(client_socket)

    

def queue_audio(sender): # send data to clients connected to the server
    send_queue_1 = queue.Queue(50000)
    send_queue_2 = queue.Queue(50000)

    stream_queues.append([sender.getpeername()[0],send_queue_1])     #stream_queues = [[ip,queue],....]
    stream_queues.append([sender.getpeername()[0],send_queue_2])

    while True:
        data = sender.recv(21600)
        send_queue_1.put(data)
        send_queue_2.put(data)



def forward_audio(sender,receiver): # send data to clients connected to the server
    queue = get_queue_from_sender(sender)
    while True:    
        data = queue.get()
        receiver.sendall(data)


def find_socket_receiver_client(socket):
    for client in receivers_clients_sockets:
        if client.getpeername()[0] == socket.getpeername()[0]:
            return client

def have_not_same_ip(socket,client):
    if socket.getpeername()[0]  != client.getpeername()[0]:
        return True

def is_not_receiving(socket):
    for client in temporary_receivers:
        if client.getpeername()[0] == socket.getpeername()[0]:
            return False
    return True

def get_queue_from_sender(sender):
    for queue in stream_queues:
        if queue[0] == sender.getpeername()[0]:
            stream_queues.remove(queue)
            return queue[1]

def queue_cleaner():
    while True:
        for queue in stream_queues:
            queue[1].get()

def main():
    ip = sys.argv[1]
    thread_connection_1 = threading.Thread(target = create_socket_sender, args=(5000,ip)).start()
    thread_connection_2 = threading.Thread(target = create_socket_sender, args=(5002,ip)).start()
    thread_connection_5 = threading.Thread(target = create_socket_receiver, args=(5001,ip)).start()
    thread_connection_6 = threading.Thread(target = queue_cleaner).start()


if __name__ == "__main__":
    main()