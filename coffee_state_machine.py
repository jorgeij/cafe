from stmpy import Machine, Driver
from threading import Thread
import paho.mqtt.client as mqtt
import coffee_user_detector
import client
import time


class MQTT_Client_1:

    def __init__(self):
        self.count = 0
        self.client = mqtt.Client()
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message

    def on_connect(self, client, userdata, flags, rc):
        print("on_connect(): {}".format(mqtt.connack_string(rc)))

    def on_message(self, client, userdata, msg):
        print("on_message(): topic: {}".format(msg.topic))

    def start(self, broker, port):
        print("Connecting to {}:{}".format(broker, port))
        self.client.connect(broker, port)
        self.client.subscribe("ttm4115/team08/movement")

        try:
            thread = Thread(target=self.client.loop_forever)
            thread.start()
        except KeyboardInterrupt:
            print("Interrupted")
            self.client.disconnect()


class CoffeeClient:
    myclient = MQTT_Client_1()

    def __init__(self):
        print('init')

    def print_state_idle(self):
        print('idle')

    def print_state_on(self):
        print('on')

    def connect_mqtt(self):
        broker, port = "mqtt.item.ntnu.no", 1883
        coffee_client.mqtt_client = self.myclient.client
        self.myclient.stm_driver = driver
        driver.start()
        self.myclient.start(broker, port)

    def detection(self):
        detector = coffee_user_detector.motionDetection()
        time.sleep(5)
        self.mqtt_client.publish('ttm4115/team08/invitation', 'inviting all users')
        self.myclient.stm_driver.send('movement_detected', 'coffee_client')

    def run_video(self):
        print("Execute video")
        client.main()


coffee_client = CoffeeClient()

# Transitions
t0 = {'source': 'initial',
      'target': 'idle',
      'effect': '__init__; connect_mqtt'}

t1 = {'trigger': 'movement_detected',
      'source': 'idle',
      'target': 'on'}

t2 = {'trigger': 't',
      'source': 'on',
      'target': 'idle'}


# States
idle = {'name': 'idle',
        'entry': 'print_state_idle; detection'}

on = {'name': 'on',
      'entry': 'print_state_on; start_timer("t", 600000); run_video'}


# Pass the set of states to the state machine
machine = Machine(name='coffee_client', transitions=[t0, t1, t2], obj=coffee_client, states=[idle, on])
coffee_client.stm = machine

driver = Driver()
driver.add_machine(machine)
driver.start()