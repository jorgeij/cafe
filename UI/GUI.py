from appJar import gui
from PIL import Image
from matplotlib.pyplot import fill
from pyparsing import col
import stmpy as stm
import logging
import paho.mqtt.client as MQTTClient

import socket
import cv2
import pickle
import struct
import threading 
import queue
import time
import pyaudio


class GUI():

    def __init__(self, stm: stm.Machine, mqtt: MQTTClient):

        # Initlize variables
        self.logger = logging.getLogger(__name__)
        self.stm = stm
        self.mqtt = mqtt
        self.frames = {"LOGIN": 0, "MAIN": 1, "VIDEO": 2}


        # Start application and frame stack
        self.app = gui("Coffee Break", "450x450")
        self.app.setPadding([20,20]) 
        self.app.setInPadding([40,20])
        self.app.startFrameStack("UI", start=self.frames['LOGIN'])
        self.create_login() 
        self.status_page()
        self.app.stopFrameStack()

    def start(self):
        self.app.go()

    l = []
    def add_name(self, appended_name):
        self.l.append(str(appended_name))
        self.app.updateListBox("list", self.l)
    
    name = ""

    #def remove_name(self):
        #self.l.remove(self.name + " is available")
        #self.app.updateListBox("list", self.l)

    def button_press(self, btn):
        self.logger.debug("Pressed button {btn}")

        if (btn == "ENTRY"):
            if(self.app.getEntry("Name") != ""):
                self.name = self.app.getEntry("Name")
                print('Say welcome to "{}".'.format(self.app.getEntry('Name')))
                self.app.selectFrame("UI", self.frames['MAIN'])
                self.stm.send('enter')
                
            else:
                self.app.errorBox("Failed login", "Please fill in username")
            

        elif (btn == "Available"):
            self.app.reloadImage("reload", "./UI/img/available.gif")
            self.app.setPadding([10,10]) 
            self.stm.send("set_available")

        elif (btn == "Unavailable"):
            self.app.reloadImage("reload", "./UI/img/unavailable.gif")
            self.app.setPadding([10,10]) 
            self.stm.send("set_unavailable")

        elif (btn == "Accept Invitation"):
            self.stm.send("accept_invite")
            self.mqtt.publish('ttm4115/team08/accept', 'accepting invitation')
            self.app.hideSubWindow("Incoming Call")


        elif (btn == "Send invite to all"):
            self.stm.send("create_invitation")
            self.mqtt.publish('ttm4115/team08/invitation', 'inviting all users')    

        elif (btn == "Decline Invitation"):
            self.stm.send("decline_invite")
            self.app.hideSubWindow("Incoming Call")

        elif (btn == "Disconnect"):
            #self.stm.send("decline_invite")
            self.app.hideSubWindow("Video control")
            self.stm.send("hangup_call")



    def create_login(self):

        # Resize image to grid
        # image = Image.open('./UI/img/logo.gif')
        # new_image = image.resize((200, 200))
        # new_image.save('./UI/img/logo_resized.gif')

        # Start frame stack
        self.app.startFrame("LOGIN")
        self.app.addImage("coffee", "./UI/img/logo_resized.gif", row=0, column=0, colspan=3)
        self.app.setImageSize("coffee", 150, 150)
       

        #self.app.addLabel("Name", "Username:", row=2,colspan=1)
        self.app.addEntry("Name", row=1)
        self.app.setEntryDefault("Name", "Username")
        self.app.addButton("ENTRY", self.button_press, row=3,colspan=3, rowspan=1)


        self.app.stopFrame()

   
    # This is used for test calling
    def show_subwindow(self,win):
       self.app.showSubWindow(win)
    
    def hide_subwindow(self,win):
       self.app.hideSubWindow(win)

    def status_page(self):
        # Start frame stack
        self.app.startFrame("STATUS")
        self.app.addImage("reload", "./UI/img/available.gif", 0,0,3)
        self.app.setPadding([10,0]) 
        self.stm.send("set_available")
       
        # Create loggin page
        self.app.addLabel(self.app.getEntry('Name'))
        self.app.addButton("Available", self.button_press, row=1, column=0,colspan=1)
        self.app.addButton("Unavailable", self.button_press, row=1, column=1,colspan=1)
        self.app.addListBox("list", self.l, colspan=3)
        self.app.addButton("Send invite to all", self.button_press, row=3, column=0,colspan=3)
        self.app.setPadding([20,60]) 
      
        # Pop up window for call
        self.app.startSubWindow("Incoming Call", modal=True)
        self.app.setSize(300, 300)
        self.app.addLabel("l1", "Incoming group call!", 0,0,3)
        self.app.addButton("Accept Invitation", self.button_press, row=2, column=0,colspan=1)
        self.app.addButton("Decline Invitation", self.button_press, row=2, column=1,colspan=1)
        self.app.stopSubWindow()

        # Control menu
        self.app.startSubWindow("Video control", modal=True)
        self.app.setSize(100, 100)
        self.app.addButton("Disconnect", self.button_press, row=2, column=0,colspan=1)
        self.app.stopSubWindow()
        

        
        self.app.stopFrame()
        